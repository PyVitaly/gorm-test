package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/PyVitaly/gorm-test/migrations"
	"gitlab.com/PyVitaly/gorm-test/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)

	databaseURL := os.Getenv("DATABASE_URL")
	natsURL := os.Getenv("NATS_URL")

	run(databaseURL, natsURL)
}

func run(databaseURL, natsURL string) {
	db, err := gorm.Open(postgres.New(
		postgres.Config{
			DSN:                  databaseURL,
			PreferSimpleProtocol: true, // disables implicit prepared statement usage
		}), &gorm.Config{})

	if err != nil {
		logrus.WithError(err).Panic("gorm")
	}

	if err = migrations.Migrate(db); err != nil {
		logrus.WithError(err).Panic("gormigrate")
	}

	//db.Joins("Company").First(&user, 1)

	company := models.Company{Name: "test"}

	db.Create(&company)

	user := models.User{Name: "test user", CompanyID: company.ID}

	db.Create(&user)

	number := models.UserPhone{Number: "1234", UserID: user.ID}

	db.Create(&number)

	//var user models.User
	//
	//db.Model(&models.User{}).Preload("Phones").Joins("Company").First(&user, 7)

	logrus.Info(user)
}
