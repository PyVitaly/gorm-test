package models

import "gorm.io/gorm"

type Company struct {
	gorm.Model
	ID   int
	Name string
}
