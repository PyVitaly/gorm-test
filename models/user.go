package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name      string
	CompanyID int
	Company   Company     `gorm:"foreignKey:CompanyID"`
	Phones    []UserPhone `gorm:"foreignKey:UserID"`
}

type UserPhone struct {
	gorm.Model
	Number string
	UserID uint
}
