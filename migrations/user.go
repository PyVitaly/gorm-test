package migrations

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gitlab.com/PyVitaly/gorm-test/models"
	"gorm.io/gorm"
)

func CreateUsersTable() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "2_create_users_table",
		Migrate: func(tx *gorm.DB) error {
			return tx.AutoMigrate(&models.User{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("users")
		},
	}
}

func AddUserPhoneTable() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "3_create_user_phones_table",
		Migrate: func(tx *gorm.DB) error {
			if err := tx.AutoMigrate(&models.UserPhone{}); err != nil {
				return err
			}

			return nil
		},
		Rollback: func(tx *gorm.DB) error {
			if err := tx.Migrator().DropColumn(&models.User{}, "phones"); err != nil {
				return err
			}

			return nil
		},
	}
}
