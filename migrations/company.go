package migrations

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gitlab.com/PyVitaly/gorm-test/models"
	"gorm.io/gorm"
)

func CreateCompaniesTable() *gormigrate.Migration {
	return &gormigrate.Migration{
		ID: "1_create_companies_table",
		Migrate: func(tx *gorm.DB) error {
			return tx.AutoMigrate(&models.Company{})
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Migrator().DropTable("companies")
		},
	}
}
