package migrations

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) error {
	migrate := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		CreateCompaniesTable(),
		CreateUsersTable(),
		AddUserPhoneTable(),
	})

	if err := migrate.Migrate(); err != nil {
		return err
	}

	return nil
}
